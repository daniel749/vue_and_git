import Vue from "vue";
import VueRouter from "vue-router";
// import Home from '../views/Home.vue'
import EventsPage from "../views/EventsPage.vue";
import About from "../views/About.vue";
import PrimeFaces from "../views/PrimeFaces.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "EventsPage",
    component: EventsPage,
  },
  {
    path: "/primefaces",
    name: "PrimeFaces",
    component: PrimeFaces,
  },
  {
    path: "/about",
    name: "About",
    component: About,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
