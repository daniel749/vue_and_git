import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import router from "./router";

import PrimeVue from "primevue/config";

import "primevue/resources/themes/saga-blue/theme.css"; //theme
import "primevue/resources/primevue.min.css"; //core css
import "primeicons/primeicons.css"; //icons

Vue.config.productionTip = false;

new Vue({
  vuetify,
  router,
  PrimeVue,
  render: (h) => h(App),
}).$mount("#app");
